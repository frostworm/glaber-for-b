/*
** Copyright Glaber 2018-2023
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

#include "zbxcommon.h"
#include "zbxalgo.h"
#include "../zbxcacheconfig/dbsync.h"
#include "../zbxcacheconfig/dbconfig.h"


typedef struct{
	u_int64_t triggerid;
} conf_trigger_t;

typedef struct {
    elems_hash_t triggers;
    mem_funcs_t *memf;
} config_t;

static config_t *conf = {0};

ELEMS_CREATE(trigger_create_cb) {
    elem->data = memf->malloc_func(NULL, sizeof(conf_trigger_t));
    bzero(elem->data, sizeof(conf_trigger_t));
}

ELEMS_FREE(trigger_free_cb) {
    conf_trigger_t *tr = elem->data;
	HALT_HERE("Trigger cleanup should be implemented");
}

void conf_triggers_init(mem_funcs_t *memf) {
    
    conf = memf->malloc_func(NULL, sizeof(config_t));
    conf->memf = memf;

   // zbx_hashset_create_ext(&conf->ref_items, 1000, ZBX_DEFAULT_UINT64_HASH_FUNC, ZBX_DEFAULT_UINT64_COMPARE_FUNC, NULL, 
   //             memf->malloc_func, memf->realloc_func, memf->free_func);

    conf->triggers = elems_hash_init(memf, trigger_create_cb, trigger_free_cb);

}


ELEMS_CALLBACK(item_update_cb) {
    return SUCCEED;
}
