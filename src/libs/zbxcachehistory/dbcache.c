/*
** Zabbix
** Copyright (C) 2001-2023 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

#include "zbxcachehistory.h"
#include "log.h"
#include "zbxmutexs.h"
#include "zbxserver.h"
#include "glb_events.h"
#include "zbxmodules.h"
#include "module.h"
#include "zbxexport.h"
#include "zbxnix.h"
#include "zbxtrends.h"
#include "zbxnum.h"
#include "zbxsysinfo.h"
#include "zbx_host_constants.h"
#include "zbx_trigger_constants.h"
#include "zbx_item_constants.h"
#include "../glb_state/glb_state_items.h"
#include "../glb_state/glb_state_triggers.h"
#include "glb_preproc.h"
#include "metric.h"
#include "../zabbix_server/dbsyncer/trends.h"
#include "zbxconnector.h"
#include "../zbxcacheconfig/dbconfig.h"

static zbx_shmem_info_t *hc_index_mem = NULL;

#define LOCK_CACHE_IDS zbx_mutex_lock(cache_ids_lock)
#define UNLOCK_CACHE_IDS zbx_mutex_unlock(cache_ids_lock)

static zbx_mutex_t cache_ids_lock = ZBX_MUTEX_NULL;

static char *sql = NULL;
static size_t sql_alloc = 4 * ZBX_KIBIBYTE;

extern unsigned char program_type;
extern int CONFIG_DOUBLE_PRECISION;

#define ZBX_IDS_SIZE 10

#define ZBX_HC_ITEMS_INIT_SIZE 1000

#define ZBX_TRENDS_CLEANUP_TIME (SEC_PER_MIN * 55)

/* the maximum time spent synchronizing history */
#define ZBX_HC_SYNC_TIME_MAX 10

/* the maximum number of items in one synchronization batch */
#define ZBX_HC_SYNC_MAX 1000
#define ZBX_HC_TIMER_MAX (ZBX_HC_SYNC_MAX / 2)
#define ZBX_HC_TIMER_SOFT_MAX (ZBX_HC_TIMER_MAX - 10)

/* the minimum processed item percentage of item candidates to continue synchronizing */
#define ZBX_HC_SYNC_MIN_PCNT 10

/* the maximum number of characters for history cache values */
#define ZBX_HISTORY_VALUE_LEN (1024 * 64)

#define ZBX_DC_FLAGS_NOT_FOR_HISTORY (ZBX_DC_FLAG_UNDEF | ZBX_DC_FLAG_NOHISTORY)
#define ZBX_DC_FLAGS_NOT_FOR_TRENDS (ZBX_DC_FLAG_UNDEF | ZBX_DC_FLAG_NOTRENDS)
#define ZBX_DC_FLAGS_NOT_FOR_MODULES (ZBX_DC_FLAGS_NOT_FOR_HISTORY | ZBX_DC_FLAG_LLD)
#define ZBX_DC_FLAGS_NOT_FOR_EXPORT (ZBX_DC_FLAG_UNDEF)

#define ZBX_HC_PROXYQUEUE_STATE_NORMAL 0
#define ZBX_HC_PROXYQUEUE_STATE_WAIT 1

typedef struct
{
	char table_name[ZBX_TABLENAME_LEN_MAX];
	zbx_uint64_t lastid;
} ZBX_DC_ID;

typedef struct
{
	ZBX_DC_ID id[ZBX_IDS_SIZE];
} ZBX_DC_IDS;

static ZBX_DC_IDS *ids = NULL;

typedef struct
{
	zbx_list_t list;
	zbx_hashset_t index;
	int state;
} zbx_hc_proxyqueue_t;

/* local history cache */
#define ZBX_MAX_VALUES_LOCAL 256
#define ZBX_STRUCT_REALLOC_STEP 8
#define ZBX_STRING_REALLOC_STEP ZBX_KIBIBYTE

#define GLB_MIN_FLUSH_VALUES 32768
#define GLB_MAX_FLUSH_TIMEOUT 3

typedef struct
{
	size_t pvalue;
	size_t len;
} dc_value_str_t;

typedef struct
{
	double value_dbl;
	zbx_uint64_t value_uint;
	dc_value_str_t value_str;
} dc_value_t;

typedef struct
{
	zbx_uint64_t itemid;
	dc_value_t value;
	zbx_timespec_t ts;
	dc_value_str_t source; /* for log items only */
//	zbx_uint64_t lastlogsize;
	int timestamp;	/* for log items only */
	int severity;	/* for log items only */
	int logeventid; /* for log items only */
	int mtime;
	unsigned char item_value_type;
	unsigned char value_type;
	unsigned char state;
	unsigned char flags; /* see ZBX_DC_FLAG_* above */
} dc_item_value_t;


ZBX_PTR_VECTOR_DECL(item_tag, zbx_tag_t)
ZBX_PTR_VECTOR_IMPL(item_tag, zbx_tag_t)
ZBX_PTR_VECTOR_IMPL(tags, zbx_tag_t *)

/******************************************************************************
 *                                                                            *
 * Purpose: update trends cache and get list of trends to flush into database *
 *                                                                            *
 * Parameters: history         - [IN]  array of history data                  *
 *             history_num     - [IN]  number of history structures           *
 *             trends          - [OUT] list of trends to flush into database  *
 *             trends_num      - [OUT] number of trends                       *
 *             compression_age - [IN]  history compression age                *
 *                                                                            *
 ******************************************************************************/
static void DCmass_update_trends(ZBX_DC_HISTORY *history, int history_num)
{
	int i; 

	for (i = 0; i < history_num; i++)
	{
		ZBX_DC_HISTORY *h = &history[i];

		if (0 != (ZBX_DC_FLAGS_NOT_FOR_TRENDS & h->metric.flags))
			continue;

		trends_account_metric(h);
	}

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
}

typedef struct
{
	zbx_uint64_t hostid;
	zbx_vector_ptr_t groups;
} zbx_host_info_t;



typedef struct
{
	zbx_uint64_t itemid;
	char *name;
	zbx_history_sync_item_t *item;
	zbx_vector_tags_t	item_tags;
} zbx_item_info_t;





/******************************************************************************
 *                                                                            *
 * Purpose: 1) calculate changeset of trigger fields to be updated            *
 *          2) generate events                                                *
 *                                                                            *
 * Parameters: trigger - [IN] the trigger to process                          *
 *             diffs   - [OUT] the vector with trigger changes                *
 *                                                                            *
 * Return value: SUCCEED - trigger processed successfully                     *
 *               FAIL    - no changes                                         *
 *                                                                            *
 * Comments: Trigger dependency checks will be done during event processing.  *
 *                                                                            *
 * Event generation depending on trigger value/state changes:                 *
 *                                                                            *
 * From \ To  | OK         | OK(?)      | PROBLEM    | PROBLEM(?) | NONE      *
 *----------------------------------------------------------------------------*
 * OK         | .          | I          | E          | I          | .         *
 *            |            |            |            |            |           *
 * OK(?)      | I          | .          | E,I        | -          | I         *
 *            |            |            |            |            |           *
 * PROBLEM    | E          | I          | E(m)       | I          | .         *
 *            |            |            |            |            |           *
 * PROBLEM(?) | E,I        | -          | E(m),I     | .          | I         *
 *                                                                            *
 * Legend:                                                                    *
 *        'E' - trigger event                                                 *
 *        'I' - internal event                                                *
 *        '.' - nothing                                                       *
 *        '-' - should never happen                                           *
 *                                                                            *
 ******************************************************************************/
static int zbx_process_trigger(struct _DC_TRIGGER *trigger, zbx_vector_ptr_t *diffs, ZBX_DC_HISTORY *history)
{
	LOG_DBG("In %s() triggerid:" ZBX_FS_UI64 " value:%d new_value:%d",
			__func__, trigger->triggerid, trigger->value, trigger->new_value);

	DEBUG_TRIGGER(trigger->triggerid, "Processing trigger, value is %d, new value is %d", trigger->value, trigger->new_value);

	// updating state anyway
	zbx_append_trigger_diff(diffs, trigger->triggerid, trigger->priority, ZBX_FLAGS_TRIGGER_DIFF_UPDATE, trigger->new_value,
							trigger->timespec.sec, trigger->new_error);

	if (trigger->new_value == TRIGGER_VALUE_OK || trigger->new_value == TRIGGER_VALUE_PROBLEM)
	{
		// creating recovery/problem event, to handle existing/not yet existing problems if they already are, the event will be discarede

		DEBUG_TRIGGER(trigger->triggerid, "Creating event for the trigger %ld create for item %ld",
					  trigger->triggerid, history[trigger->history_idx].metric.itemid);

		zbx_add_event(EVENT_SOURCE_TRIGGERS, EVENT_OBJECT_TRIGGER, trigger->triggerid,
					  &trigger->timespec, trigger->new_value, trigger->description,
					  trigger->expression, trigger->recovery_expression,
					  trigger->priority, trigger->type, &trigger->tags,
					  trigger->correlation_mode, trigger->correlation_tag, trigger->value, trigger->opdata,
					  trigger->event_name, NULL, trigger->history_idx);
	}

	// if state hasn't changed, and not problem in multi-problem gen config, nothing to do
	if (trigger->value == trigger->new_value)
	{
		if (TRIGGER_VALUE_PROBLEM != trigger->new_value ||
			TRIGGER_TYPE_MULTIPLE_TRUE != trigger->type)
			return SUCCEED;
	}

	if (TRIGGER_VALUE_UNKNOWN == trigger->value || TRIGGER_VALUE_UNKNOWN == trigger->new_value)
	{
		DEBUG_TRIGGER(trigger->triggerid, "Creating internal event for the trigger");

		zbx_add_event(EVENT_SOURCE_INTERNAL, EVENT_OBJECT_TRIGGER, trigger->triggerid,
					  &trigger->timespec, trigger->new_value, NULL, trigger->expression,
					  trigger->recovery_expression, 0, 0, &trigger->tags, 0, NULL, 0, NULL, NULL,
					  trigger->error, trigger->history_idx);
	}

	if (trigger->new_value == TRIGGER_VALUE_UNKNOWN)
		return SUCCEED;

	return SUCCEED;
}

/******************************************************************************
 *                                                                            *
 * Comments: helper function for zbx_process_triggers()                       *
 *                                                                            *
 ******************************************************************************/
static int zbx_trigger_topoindex_compare(const void *d1, const void *d2)
{
	const DC_TRIGGER *t1 = *(const DC_TRIGGER *const *)d1;
	const DC_TRIGGER *t2 = *(const DC_TRIGGER *const *)d2;

	ZBX_RETURN_IF_NOT_EQUAL(t1->topoindex, t2->topoindex);

	return 0;
}

/******************************************************************************
 *                                                                            *
 * Purpose: process triggers - calculates property changeset and generates    *
 *          events                                                            *
 *                                                                            *
 * Parameters: triggers     - [IN] the triggers to process                    *
 *             trigger_diff - [OUT] the trigger changeset                     *
 *                                                                            *
 * Comments: The trigger_diff changeset must be cleaned by the caller:        *
 *                zbx_vector_ptr_clear_ext(trigger_diff,                      *
 *                              (zbx_clean_func_t)zbx_trigger_diff_free);     *
 *                                                                            *
 ******************************************************************************/
static void zbx_process_triggers(zbx_vector_ptr_t *triggers, zbx_vector_ptr_t *trigger_diff, ZBX_DC_HISTORY *history)
{
	int i;

	zabbix_log(LOG_LEVEL_DEBUG, "In %s() values_num:%d", __func__, triggers->values_num);

	if (0 == triggers->values_num)
		goto out;

	zbx_vector_ptr_sort(triggers, zbx_trigger_topoindex_compare);

	for (i = 0; i < triggers->values_num; i++)
		zbx_process_trigger((struct _DC_TRIGGER *)triggers->values[i], trigger_diff, history);

	zbx_vector_ptr_sort(trigger_diff, ZBX_DEFAULT_UINT64_PTR_COMPARE_FUNC);
out:
	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
}

/******************************************************************************
 *                                                                            *
 * Purpose: re-calculate and update values of triggers related to the items   *
 *                                                                            *
 * Parameters: history           - [IN] array of history data                 *
 *             history_num       - [IN] number of history structures          *
 *             history_itemids   - [IN] the item identifiers                  *
 *                                      (used for item lookup)                *
 *             history_items     - [IN] the items                             *
 *             history_errcodes  - [IN] item error codes                      *
 *             timers            - [IN] the trigger timers                    *
 *             trigger_diff      - [OUT] trigger updates                      *
 *                                                                            *
 ******************************************************************************/
static int recalculate_triggers(ZBX_DC_HISTORY *history, int history_num,
								 const zbx_vector_uint64_t *history_itemids, 
								 const zbx_history_sync_item_t *history_items, const int *history_errcodes,
								 const zbx_vector_ptr_t *timers, zbx_vector_ptr_t *trigger_diff)
{
	int i, item_num = 0, timers_num = 0, trigger_num = 0;
	zbx_uint64_t *itemids = NULL;
	zbx_timespec_t *timespecs = NULL;
	zbx_hashset_t trigger_info;
	zbx_vector_ptr_t trigger_order;
	zbx_vector_ptr_t trigger_items;

	LOG_DBG("In %s()", __func__);

	if (0 != history_num)
	{
		itemids = (zbx_uint64_t *)zbx_malloc(itemids, sizeof(zbx_uint64_t) * (size_t)history_num);
		timespecs = (zbx_timespec_t *)zbx_malloc(timespecs, sizeof(zbx_timespec_t) * (size_t)history_num);

		for (i = 0; i < history_num; i++)
		{
			ZBX_DC_HISTORY *h = &history[i];

			itemids[item_num] = h->metric.itemid;
			timespecs[item_num] = h->metric.ts;
			item_num++;
		}
	}

	for (i = 0; i < timers->values_num; i++)
	{
		zbx_trigger_timer_t *timer = (zbx_trigger_timer_t *)timers->values[i];

		timers_num++;
	}

	if (0 == item_num && 0 == timers_num)
		goto out;

	zbx_hashset_create(&trigger_info, MAX(100, 2 * item_num + timers_num),
					   ZBX_DEFAULT_UINT64_HASH_FUNC, ZBX_DEFAULT_UINT64_COMPARE_FUNC);

	zbx_vector_ptr_create(&trigger_order);
	zbx_vector_ptr_reserve(&trigger_order, trigger_info.num_slots);

	zbx_vector_ptr_create(&trigger_items);

	if (0 != item_num)
	{
		zbx_dc_config_history_sync_get_triggers_by_itemids(&trigger_info, &trigger_order, itemids, timespecs, item_num);
		zbx_prepare_triggers((DC_TRIGGER **)trigger_order.values, trigger_order.values_num);
	//	zbx_determine_items_in_expressions(&trigger_order, itemids, item_num);
	}

	if (0 != timers_num)
	{
		int offset = trigger_order.values_num;

		zbx_dc_get_triggers_by_timers(&trigger_info, &trigger_order, timers);

		if (offset != trigger_order.values_num)
		{
			zbx_prepare_triggers((DC_TRIGGER **)trigger_order.values + offset,
								 trigger_order.values_num - offset);
		}
	}

	zbx_vector_ptr_sort(&trigger_order, ZBX_DEFAULT_UINT64_PTR_COMPARE_FUNC);
	zbx_evaluate_expressions(&trigger_order, history_itemids, history_items, history_errcodes);
	zbx_process_triggers(&trigger_order, trigger_diff, history);
	
	trigger_num = trigger_order.values_num;

	DCfree_triggers(&trigger_order);

	zbx_vector_ptr_destroy(&trigger_items);

	zbx_hashset_destroy(&trigger_info);
	zbx_vector_ptr_destroy(&trigger_order);
out:
	zbx_free(timespecs);
	zbx_free(itemids);
	
	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);

	return trigger_num;
}

static void DCinventory_value_add(zbx_vector_ptr_t *inventory_values, const zbx_history_sync_item_t *item,
								  ZBX_DC_HISTORY *h)
{
	char value[MAX_BUFFER_LEN];
	const char *inventory_field;
	zbx_inventory_value_t *inventory_value;

	if (VARIANT_VALUE_ERROR == h->metric.value.type)
		return;

	if (HOST_INVENTORY_AUTOMATIC != item->host.inventory_mode)
		return;

	if (0 != (ZBX_DC_FLAG_UNDEF & h->metric.flags) || 0 != (VARIANT_VALUE_NONE == h->metric.value.type) ||
		NULL == (inventory_field = zbx_db_get_inventory_field(item->inventory_link)))
	{
		return;
	}

	zbx_snprintf(value, MAX_BUFFER_LEN, "%s", zbx_variant_value_desc(&h->metric.value));
	zbx_format_value(value, sizeof(value), item->valuemapid, ZBX_NULL2EMPTY_STR(item->units), h->hist_value_type);

	inventory_value = (zbx_inventory_value_t *)zbx_malloc(NULL, sizeof(zbx_inventory_value_t));

	inventory_value->hostid = item->host.hostid;
	inventory_value->idx = item->inventory_link - 1;
	inventory_value->field_name = inventory_field;
	inventory_value->value = zbx_strdup(NULL, value);

	zbx_vector_ptr_append(inventory_values, inventory_value);
}

static void DCadd_update_inventory_sql(size_t *sql_offset, const zbx_vector_ptr_t *inventory_values)
{
	char *value_esc;
	int i;

	for (i = 0; i < inventory_values->values_num; i++)
	{
		const zbx_inventory_value_t *inventory_value = (zbx_inventory_value_t *)inventory_values->values[i];

		value_esc = zbx_db_dyn_escape_field("host_inventory", inventory_value->field_name, inventory_value->value);

		zbx_snprintf_alloc(&sql, &sql_alloc, sql_offset,
						   "update host_inventory set %s='%s' where hostid=" ZBX_FS_UI64 ";\n",
						   inventory_value->field_name, value_esc, inventory_value->hostid);

		zbx_db_execute_overflowed_sql(&sql, &sql_alloc, sql_offset);

		zbx_free(value_esc);
	}
}

static void DCinventory_value_free(zbx_inventory_value_t *inventory_value)
{
	zbx_free(inventory_value->value);
	zbx_free(inventory_value);
}

/******************************************************************************
 *                                                                            *
 * Purpose: frees resources allocated to store str/text/log value             *
 *                                                                            *
 * Parameters: history     - [IN] the history data                            *
 *             history_num - [IN] the number of values in history data        *
 *                                                                            *
 ******************************************************************************/
static void dc_history_clean_value(ZBX_DC_HISTORY *history)
{
	zbx_variant_clear(&history->metric.value);
}

/******************************************************************************
 *                                                                            *
 * Purpose: frees resources allocated to store str/text/log values            *
 *                                                                            *
 * Parameters: history     - [IN] the history data                            *
 *             history_num - [IN] the number of values in history data        *
 *                                                                            *
 ******************************************************************************/
static void hc_free_item_values(ZBX_DC_HISTORY *history, int history_num)
{
	int i;

	for (i = 0; i < history_num; i++) 
		dc_history_clean_value(&history[i]);
}



/******************************************************************************
converts metric to appropriate history type
 ******************************************************************************/
static void normalize_and_convert_metric_value(ZBX_DC_HISTORY *h)
{
	char buff_str[MAX_STRING_LEN];

	if ( VARIANT_VALUE_NONE == h->metric.value.type || 
		 VARIANT_VALUE_ERROR == h->metric.value.type)// ITEM_STATE_NOTSUPPORTED == hdata->state)
		return;

	DEBUG_ITEM(h->metric.itemid, "in Normalizing item, state is SUPPORTED");
	switch (h->hist_value_type) {
		case ITEM_VALUE_TYPE_STR:
		case ITEM_VALUE_TYPE_TEXT:
		case ITEM_VALUE_TYPE_LOG:
			zbx_variant_convert(&h->metric.value, VARIANT_VALUE_STR); 
			break;
		case ITEM_VALUE_TYPE_UINT64:
			if (FAIL == zbx_variant_convert(&h->metric.value, VARIANT_VALUE_UINT64)) {
				zbx_snprintf(buff_str, MAX_STRING_LEN, "Cannot convert value '%s' to UINT64 value",
						zbx_variant_value_desc(&h->metric.value));

				DEBUG_ITEM(h->metric.itemid, "%s", buff_str);		
				glb_state_item_set_error(h->metric.itemid, buff_str);
				zbx_variant_clear(&h->metric.value);
				zbx_variant_set_error(&h->metric.value, zbx_strdup(NULL,buff_str));
				h->hist_value_type = ITEM_VALUE_TYPE_NONE;
			}
			break;
		case ITEM_VALUE_TYPE_FLOAT:
			if (FAIL == zbx_variant_convert(&h->metric.value, VARIANT_VALUE_DBL)) {
				zbx_snprintf(buff_str, MAX_STRING_LEN, "Cannot convert value '%s' to DOUBLE value",
						zbx_variant_value_desc(&h->metric.value));

				DEBUG_ITEM(h->metric.itemid, "%s", buff_str);		
				glb_state_item_set_error(h->metric.itemid, buff_str);
				zbx_variant_clear(&h->metric.value);
				zbx_variant_set_error(&h->metric.value, zbx_strdup(NULL, buff_str));
				h->hist_value_type = ITEM_VALUE_TYPE_NONE;
			}
			break;
		default: 
			THIS_SHOULD_NEVER_HAPPEN;
	}
}

/******************************************************************************
 *                                                                            *
 * Purpose: helper function for DCmass_proxy_add_history()                    *
 *                                                                            *
 * Comment: this function is meant for items with value_type other than       *
 *          ITEM_VALUE_TYPE_LOG not containing meta information in result     *
 *                                                                            *
 ******************************************************************************/
static void dc_add_proxy_history(ZBX_DC_HISTORY *history, int history_num)
{
	int i, now, history_count = 0;
	unsigned int flags;
	zbx_db_insert_t db_insert;

	now = (int)time(NULL);
	zbx_db_insert_prepare(&db_insert, "proxy_history", "itemid", "clock", "ns", "value", "flags", "write_clock",
						  (char *)NULL);

	for (i = 0; i < history_num; i++)
	{
		const ZBX_DC_HISTORY *h = &history[i];

		if (0 != (h->metric.flags & ZBX_DC_FLAG_UNDEF))
			continue;

		if (0 != (h->metric.flags & ZBX_DC_FLAG_META))
			continue;

		if ( VARIANT_VALUE_ERROR == h->metric.value.type)
			continue;

		if ( VARIANT_VALUE_NONE != h->metric.value.type)
			flags = 0;
		else
		{
			flags = PROXY_HISTORY_FLAG_NOVALUE;
		}

		history_count++;
		zbx_db_insert_add_values(&db_insert, h->metric.itemid, h->metric.ts.sec, h->metric.ts.ns, 
												zbx_variant_value_desc(&h->metric.value), flags, now);
	}

	zbx_db_insert_execute(&db_insert);
	zbx_db_insert_clean(&db_insert);
}


/******************************************************************************
 *                                                                            *
 * Purpose: helper function for DCmass_proxy_add_history()                    *
 *                                                                            *
 ******************************************************************************/
static void dc_add_proxy_history_notsupported(ZBX_DC_HISTORY *history, int history_num)
{
	int i, now, history_count = 0;
	zbx_db_insert_t db_insert;

	now = (int)time(NULL);
	zbx_db_insert_prepare(&db_insert, "proxy_history", "itemid", "clock", "ns", "value", "state", "write_clock",
						   (char *)NULL);

	for (i = 0; i < history_num; i++)
	{
		const ZBX_DC_HISTORY *h = &history[i];

		if (VARIANT_VALUE_ERROR != h->metric.value.type)
			continue;

		history_count++;
		zbx_db_insert_add_values(&db_insert, h->metric.itemid, h->metric.ts.sec, h->metric.ts.ns, ZBX_NULL2EMPTY_STR(h->metric.value.data.str),
								 (int)ITEM_STATE_NOTSUPPORTED, now);
	}

//	change_proxy_history_count(history_count);
	zbx_db_insert_execute(&db_insert);
	zbx_db_insert_clean(&db_insert);
}

/******************************************************************************
 *                                                                            *
 * Purpose: inserting new history data after new value is received            *
 *                                                                            *
 * Parameters: history     - array of history data                            *
 *             history_num - number of history structures                     *
 *                                                                            *
 ******************************************************************************/
static void DBmass_proxy_add_history(ZBX_DC_HISTORY *history, int history_num)
{
	int i, h_num = 0, //h_meta_num = 0,
	 hlog_num = 0, notsupported_num = 0;

	zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

	for (i = 0; i < history_num; i++)
	{
		const ZBX_DC_HISTORY *h = &history[i];

		if (VARIANT_VALUE_ERROR == h->metric.value.type)
		{
			notsupported_num++;
			continue;
		}

		switch (h->hist_value_type)
		{
		case ITEM_VALUE_TYPE_LOG:
			hlog_num++;
			break;
		case ITEM_VALUE_TYPE_FLOAT:
		case ITEM_VALUE_TYPE_UINT64:
		case ITEM_VALUE_TYPE_STR:
		case ITEM_VALUE_TYPE_TEXT:
		//	if (0 != (h->metric.flags & ZBX_DC_FLAG_META))
		//		h_meta_num++;
		//	else
				h_num++;
			break;
		case ITEM_VALUE_TYPE_NONE:
			h_num++;
			break;
		default:
			THIS_SHOULD_NEVER_HAPPEN;
		}
	}

	//if (0 != h_num)
		dc_add_proxy_history(history, history_num);

//	if (0 != h_meta_num)
//		dc_add_proxy_history_meta(history, history_num);

	// if (0 != hlog_num)
	// 	dc_add_proxy_history_log(history, history_num);

	if (0 != notsupported_num)
		dc_add_proxy_history_notsupported(history, history_num);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
}

/******************************************************************************
 *                                                                            *
 * Purpose: prepare history data using items from configuration cache and     *
 *          generate item changes to be applied and host inventory values to  *
 *          be added                                                          *
 *                                                                            *
 * Parameters: history             - [IN/OUT] array of history data           *
 *             itemids             - [IN] the item identifiers                *
 *                                        (used for item lookup)              *
 *             items               - [IN] the items                           *
 *             errcodes            - [IN] item error codes                    *
 *             history_num         - [IN] number of history structures        *
 *             item_diff           - [OUT] the changes in item data           *
 *             inventory_values    - [OUT] the inventory values to add        *
 *             compression_age     - [IN] history compression age             *
 *             proxy_subscribtions - [IN] history compression age             *
 *                                                                            *
 ******************************************************************************/
static void DCmass_prepare_history(ZBX_DC_HISTORY *history, const zbx_vector_uint64_t *itemids,
								zbx_history_sync_item_t *items, const int *errcodes, int history_num,
								zbx_vector_ptr_t *inventory_values, zbx_vector_uint64_pair_t *proxy_subscribtions)
{
	int i;

	zabbix_log(LOG_LEVEL_DEBUG, "In %s() history_num:%d", __func__, history_num);

	for (i = 0; i < history_num; i++)
	{
		ZBX_DC_HISTORY *h = &history[i];
		zbx_history_sync_item_t  *item;

		int index;

		DEBUG_ITEM(h->metric.itemid, "Will do prepare history, type: '%s', value: '%s'", 
				zbx_variant_type_desc(&h->metric.value), zbx_variant_value_desc(&h->metric.value));

		if (FAIL == (index = zbx_vector_uint64_bsearch(itemids, h->metric.itemid, ZBX_DEFAULT_UINT64_COMPARE_FUNC)))
		{
			THIS_SHOULD_NEVER_HAPPEN;
			h->metric.flags |= ZBX_DC_FLAG_UNDEF;
			continue;
		}

		if (SUCCEED != errcodes[index])
		{
			DEBUG_ITEM(h->metric.itemid, "Setting undefined value flag, due to errcode");
			h->metric.flags |= ZBX_DC_FLAG_UNDEF;
			continue;
		}

		item = &items[index];

		if (ITEM_ADMIN_STATUS_ENABLED != item->admin_status || HOST_STATUS_MONITORED != item->host.status)
		{
			DEBUG_ITEM(h->metric.itemid, "Setting undefined value flag, due to item status");
			h->metric.flags |= ZBX_DC_FLAG_UNDEF;
			continue;
		}

		if (0 == item->history)
			h->metric.flags |= ZBX_DC_FLAG_NOHISTORY;

		if (0 == item->trends || (ITEM_VALUE_TYPE_FLOAT != item->value_type &&
								  ITEM_VALUE_TYPE_UINT64 != item->value_type))
			h->metric.flags |= ZBX_DC_FLAG_NOTRENDS;

		h->host_name = (char *)item->host.host;
		h->item_key = (char *)item->key_orig;
		h->hist_value_type = item->value_type;

		DEBUG_ITEM(h->metric.itemid, "Normalizing item");

		normalize_and_convert_metric_value(h);

		DEBUG_ITEM(h->metric.itemid, "Finished normalizing item, history type is %d, type: '%s' , value: '%s'", 
				h->hist_value_type, zbx_variant_type_desc(&h->metric.value), zbx_variant_value_desc(&h->metric.value));
		DCinventory_value_add(inventory_values, item, h);

		if (0 != item->host.proxy_hostid && FAIL == is_item_processed_by_server(item->type, item->key_orig))
		{
			zbx_uint64_pair_t p = {item->host.proxy_hostid, (u_int64_t)h->metric.ts.sec};
			zbx_vector_uint64_pair_append(proxy_subscribtions, p);
		}
	}

	zbx_vector_ptr_sort(inventory_values, ZBX_DEFAULT_UINT64_PTR_COMPARE_FUNC);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
}

static void DCmodule_sync_history(int history_float_num, int history_integer_num, int history_string_num,
								  int history_text_num, int history_log_num, ZBX_HISTORY_FLOAT *history_float,
								  ZBX_HISTORY_INTEGER *history_integer, ZBX_HISTORY_STRING *history_string,
								  ZBX_HISTORY_TEXT *history_text, ZBX_HISTORY_LOG *history_log)
{
	if (0 != history_float_num)
	{
		int i;

		zabbix_log(LOG_LEVEL_DEBUG, "syncing float history data with modules...");

		for (i = 0; NULL != history_float_cbs[i].module; i++)
		{
			zabbix_log(LOG_LEVEL_DEBUG, "... module \"%s\"", history_float_cbs[i].module->name);
			history_float_cbs[i].history_float_cb(history_float, history_float_num);
		}

		zabbix_log(LOG_LEVEL_DEBUG, "synced %d float values with modules", history_float_num);
	}

	if (0 != history_integer_num)
	{
		int i;

		zabbix_log(LOG_LEVEL_DEBUG, "syncing integer history data with modules...");

		for (i = 0; NULL != history_integer_cbs[i].module; i++)
		{
			zabbix_log(LOG_LEVEL_DEBUG, "... module \"%s\"", history_integer_cbs[i].module->name);
			history_integer_cbs[i].history_integer_cb(history_integer, history_integer_num);
		}

		zabbix_log(LOG_LEVEL_DEBUG, "synced %d integer values with modules", history_integer_num);
	}

	if (0 != history_string_num)
	{
		int i;

		zabbix_log(LOG_LEVEL_DEBUG, "syncing string history data with modules...");

		for (i = 0; NULL != history_string_cbs[i].module; i++)
		{
			zabbix_log(LOG_LEVEL_DEBUG, "... module \"%s\"", history_string_cbs[i].module->name);
			history_string_cbs[i].history_string_cb(history_string, history_string_num);
		}

		zabbix_log(LOG_LEVEL_DEBUG, "synced %d string values with modules", history_string_num);
	}

	if (0 != history_text_num)
	{
		int i;

		zabbix_log(LOG_LEVEL_DEBUG, "syncing text history data with modules...");

		for (i = 0; NULL != history_text_cbs[i].module; i++)
		{
			zabbix_log(LOG_LEVEL_DEBUG, "... module \"%s\"", history_text_cbs[i].module->name);
			history_text_cbs[i].history_text_cb(history_text, history_text_num);
		}

		zabbix_log(LOG_LEVEL_DEBUG, "synced %d text values with modules", history_text_num);
	}

	if (0 != history_log_num)
	{
		int i;

		zabbix_log(LOG_LEVEL_DEBUG, "syncing log history data with modules...");

		for (i = 0; NULL != history_log_cbs[i].module; i++)
		{
			zabbix_log(LOG_LEVEL_DEBUG, "... module \"%s\"", history_log_cbs[i].module->name);
			history_log_cbs[i].history_log_cb(history_log, history_log_num);
		}

		zabbix_log(LOG_LEVEL_DEBUG, "synced %d log values with modules", history_log_num);
	}
}

/******************************************************************************
 *                                                                            *
 * Purpose: prepares history update by checking which values must be stored   *
 *                                                                            *
 * Parameters: history     - [IN/OUT] the history values                      *
 *             history_num - [IN] the number of history values                *
 *                                                                            *
 ******************************************************************************/
static void proxy_prepare_history(ZBX_DC_HISTORY *history, int history_num)
{
	int i, *errcodes;
	zbx_history_sync_item_t	*items;
	zbx_vector_uint64_t itemids;

	zbx_vector_uint64_create(&itemids);
	zbx_vector_uint64_reserve(&itemids, history_num);

	for (i = 0; i < history_num; i++)
	{
		zbx_vector_uint64_append(&itemids, history[i].metric.itemid);
		DEBUG_ITEM(history[i].metric.itemid, "Processing in history sync");
	}

	items = (zbx_history_sync_item_t*)zbx_malloc(NULL, sizeof(zbx_history_sync_item_t) * (size_t)history_num);
	errcodes = (int *)zbx_malloc(NULL, sizeof(int) * (size_t)history_num);

	zbx_dc_config_history_sync_get_items_by_itemids(items, itemids.values, errcodes, (size_t)itemids.values_num,
			ZBX_ITEM_GET_SYNC);

	for (i = 0; i < history_num; i++)
	{
		if (SUCCEED != errcodes[i])
			continue;

		/* store items with enabled history  */
		if (0 != items[i].history)
			continue;

		/* store numeric items to handle data conversion errors on server and trends */
		if (ITEM_VALUE_TYPE_FLOAT == items[i].value_type || ITEM_VALUE_TYPE_UINT64 == items[i].value_type)
			continue;

		/* store discovery rules */
		if (0 != (items[i].flags & ZBX_FLAG_DISCOVERY_RULE))
			continue;

		/* store errors or first value after an error */
		// if (ITEM_STATE_NOTSUPPORTED == history[i].state || ITEM_STATE_NOTSUPPORTED == items[i].state)
		//	continue;

		/* store items linked to host inventory */
		if (0 != items[i].inventory_link)
			continue;

		dc_history_clean_value(history + i);

	}

	zbx_dc_config_clean_history_sync_items(items, errcodes, (size_t)history_num);
	zbx_free(items);
	zbx_free(errcodes);
	zbx_vector_uint64_destroy(&itemids);
}

static void metrics_proc_cb(const metric_t * metric, int i, void *cb_data) {
	
	ZBX_DC_HISTORY *history = (ZBX_DC_HISTORY*)cb_data;
	ZBX_DC_HISTORY *h = &history[i];
	bzero(h, sizeof(ZBX_DC_HISTORY));

	h->metric = *metric;

	if (NULL ==  metric->value.data.str) 
		return;
	
	if (VARIANT_VALUE_STR == h->metric.value.type) {
		zbx_variant_set_str(&h->metric.value, zbx_strdup(NULL, metric->value.data.str));
		return;
	}
	
	if (VARIANT_VALUE_ERROR == h->metric.value.type )  {
		zbx_variant_set_error(&h->metric.value, zbx_strdup(NULL, metric->value.data.str));
		return;
	}
	
	
}

static void sync_proxy_history(int *total_num, int proc_num)
{
	int history_num, txn_rc;
	time_t sync_start;
	ZBX_DC_HISTORY history[ZBX_HC_SYNC_MAX];

	sync_start = time(NULL);

	do
	{
		history_num = process_receive_metrics(proc_num, metrics_proc_cb, history, ZBX_HC_SYNC_MAX );

		if (0 == history_num)
			break;

		proxy_prepare_history(history, history_num);

		do
		{
			zbx_db_begin();
			DBmass_proxy_add_history(history, history_num);
		} while (ZBX_DB_DOWN == (txn_rc = zbx_db_commit()));

		*total_num += history_num;

		hc_free_item_values(history, history_num);

	} while (history_num > 0 && ZBX_HC_SYNC_TIME_MAX >= time(NULL) - sync_start);
}

/******************************************************************************
 *                                                                            *
 * Purpose: update item data and inventory in database                        *
 *                                                                            *
 * Parameters: item_diff        - item changes                                *
 *             inventory_values - inventory values                            *
 *                                                                            *
 ******************************************************************************/
extern int CONFIG_ENABLE_INVENTORY;

static void	DBmass_update_inventory_items(const zbx_vector_ptr_t *inventory_values)
{
	size_t	sql_offset = 0;
	int	i;

	zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
	
	//LOG_INF("Writing inventory option is %d", CONFIG_ENABLE_INVENTORY);

	if (0 != inventory_values->values_num && 0 != CONFIG_ENABLE_INVENTORY)
	{
		zbx_db_begin_multiple_update(&sql, &sql_alloc, &sql_offset);

		if (0 != inventory_values->values_num)
			DCadd_update_inventory_sql(&sql_offset, inventory_values);

		zbx_db_end_multiple_update(&sql, &sql_alloc, &sql_offset);

		if (sql_offset > 16)	/* In ORACLE always present begin..end; */
			zbx_db_execute("%s", sql);

		DCconfig_update_inventory_values(inventory_values);
	}

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
}


/******************************************************************************
 *                                                                            *
 * Purpose: flush history cache to database, process triggers of flushed      *
 *          and timer triggers from timer queue                               *
 *                                                                            *
 * Parameters: sync_timeout - [IN] the timeout in seconds                     *
 *             values_num   - [IN/OUT] the number of synced values            *
 *             triggers_num - [IN/OUT] the number of processed timers         *
 *             more         - [OUT] a flag indicating the cache emptiness:    *
 *                               ZBX_SYNC_DONE - nothing to sync, go idle     *
 *                               ZBX_SYNC_MORE - more data to sync            *
 *                                                                            *
 * Comments: This function loops syncing history values by 1k batches and     *
 *           processing timer triggers by batches of 500 triggers.            *
 *           Unless full sync is being done the loop is aborted if either     *
 *           timeout has passed or there are no more data to process.         *
 *           The last is assumed when the following is true:                  *
 *            a) history cache is empty or less than 10% of batch values were *
 *               processed (the other items were locked by triggers)          *
 *            b) less than 500 (full batch) timer triggers were processed     *
 *                                                                            *
 ******************************************************************************/
static void sync_server_history(int *values_num, int *triggers_num, int proc_num)
{

	int i, history_num, timers_num,  txn_error;

	unsigned int item_retrieve_mode;
	time_t sync_start;
	zbx_vector_uint64_t triggerids;
	zbx_vector_ptr_t trigger_diff, inventory_values, trigger_timers;
	zbx_vector_uint64_pair_t proxy_subscribtions;
	ZBX_DC_HISTORY history[ZBX_HC_SYNC_MAX];
	zbx_vector_connector_filter_t	connector_filters_history, connector_filters_events;

	item_retrieve_mode = 0 == zbx_has_export_dir() ? ZBX_ITEM_GET_SYNC : ZBX_ITEM_GET_SYNC_EXPORT;

	zbx_vector_connector_filter_create(&connector_filters_history);
	zbx_vector_connector_filter_create(&connector_filters_events);
	zbx_vector_ptr_create(&inventory_values);
	zbx_vector_ptr_create(&trigger_diff);

	zbx_vector_uint64_pair_create(&proxy_subscribtions);

	zbx_vector_uint64_create(&triggerids);
	zbx_vector_uint64_reserve(&triggerids, ZBX_HC_SYNC_MAX);

	zbx_vector_ptr_create(&trigger_timers);
	zbx_vector_ptr_reserve(&trigger_timers, ZBX_HC_TIMER_MAX);

	*values_num = 0;
	*triggers_num = 0;
	sync_start = time(NULL);

	item_retrieve_mode = 0 == zbx_has_export_dir() ? ZBX_ITEM_GET_SYNC : ZBX_ITEM_GET_SYNC_EXPORT;

	do
	{
		zbx_history_sync_item_t		*items = NULL;
		int *errcodes;
		zbx_vector_uint64_t itemids;

		zbx_vector_uint64_create(&itemids);
		zbx_vector_uint64_clear(&triggerids);
		zbx_dc_um_handle_t *um_handle;
		um_handle = zbx_dc_open_user_macros();
	
		history_num = process_receive_metrics(proc_num, metrics_proc_cb, history, ZBX_HC_SYNC_MAX );
		
		if (0 !=  history_num ) {

			items = (zbx_history_sync_item_t*)zbx_malloc(NULL, sizeof(zbx_history_sync_item_t) * (size_t)history_num);
			errcodes = (int *)zbx_malloc(NULL, sizeof(int) * (size_t)history_num);

			zbx_vector_uint64_reserve(&itemids, history_num);

			for (i = 0; i < history_num; i++)
			{
				zbx_vector_uint64_append(&itemids, history[i].metric.itemid);
				DEBUG_ITEM(history[i].metric.itemid, "Processing in history sync");
			}

			zbx_vector_uint64_sort(&itemids, ZBX_DEFAULT_UINT64_COMPARE_FUNC);
			
			zbx_dc_config_history_sync_get_items_by_itemids(items, itemids.values, errcodes,
					(size_t)history_num, item_retrieve_mode);
			
	
			DCmass_prepare_history(history, &itemids, items, errcodes, history_num,
								   &inventory_values, &proxy_subscribtions);

			glb_state_item_add_values(history, history_num);
		}

		/* don't process trigger timers when server is shutting down */
		zbx_dc_get_trigger_timers(&trigger_timers, time(NULL), ZBX_HC_TIMER_SOFT_MAX,
										  ZBX_HC_TIMER_MAX);
		
		timers_num = trigger_timers.values_num;

		if (0 != history_num || 0 != timers_num)
		{
			for (i = 0; i < trigger_timers.values_num; i++)
			{
				zbx_trigger_timer_t *timer = (zbx_trigger_timer_t *)trigger_timers.values[i];

				zbx_vector_uint64_append(&triggerids, timer->triggerid);
			}

			do
			{
				zbx_db_begin();

				*triggers_num = *triggers_num + recalculate_triggers(history, history_num, &itemids, items, errcodes,
									 &trigger_timers, &trigger_diff);

				/* process trigger events generated by recalculate_triggers() */
				zbx_process_events(&trigger_diff, &triggerids, history);
				glb_state_triggers_apply_diffs(&trigger_diff);

				if (ZBX_DB_OK != (txn_error = zbx_db_commit()))
					zbx_clean_events();

				zbx_vector_ptr_clear_ext(&trigger_diff, (zbx_clean_func_t)zbx_trigger_diff_free);
			} while (ZBX_DB_DOWN == txn_error);

			// separate story, some kind of SLA and services calculation is
			// done in the db either, should probably go to the state cache either
			if (ZBX_DB_OK == txn_error)
				zbx_events_update_itservices();
		}
		
		zbx_dc_close_user_macros(um_handle);

		if (0 != trigger_timers.values_num)
		{
			zbx_dc_reschedule_trigger_timers(&trigger_timers, time(NULL));
			zbx_vector_ptr_clear(&trigger_timers);
		}

		if (0 != proxy_subscribtions.values_num)
		{
			zbx_vector_uint64_pair_sort(&proxy_subscribtions, ZBX_DEFAULT_UINT64_COMPARE_FUNC);
			zbx_dc_proxy_update_nodata(&proxy_subscribtions);
			zbx_vector_uint64_pair_clear(&proxy_subscribtions);
		}

		// if (FAIL != ret)
		// {

		// 	if (SUCCEED == zbx_is_export_enabled(ZBX_FLAG_EXPTYPE_EVENTS))
		// 		zbx_export_events()
		// }

		if (0 != history_num || 0 != timers_num)
			zbx_clean_events();

		if (0 != history_num)
		{
			glb_history_add_history(history, history_num);
			DCmass_update_trends(history, history_num);

			do
			{
				zbx_db_begin();

				zbx_process_events(NULL, NULL, NULL);

				if (ZBX_DB_OK != (txn_error = zbx_db_commit()))
					zbx_reset_event_recovery();
				
				DBmass_update_inventory_items(&inventory_values);

			} while (ZBX_DB_DOWN == txn_error);

			zbx_clean_events();
			zbx_vector_ptr_clear_ext(&inventory_values, (zbx_clean_func_t)DCinventory_value_free);


			zbx_dc_config_clean_history_sync_items(items, errcodes, (size_t)history_num);

			zbx_free(errcodes);
			zbx_free(items);

			hc_free_item_values(history, history_num);

		}

		zbx_vector_uint64_destroy(&itemids);
		*values_num += history_num;

		/* Exit from sync loop if we have spent too much time here.       */
		/* This is done to allow syncer process to update its statistics. */
	} while ( ZBX_IS_RUNNING() &&
			  (history_num + triggerids.values_num + timers_num) > 0  && 
			  ZBX_HC_SYNC_TIME_MAX >= time(NULL) - sync_start );

	zbx_vector_ptr_destroy(&inventory_values);
	zbx_vector_ptr_destroy(&trigger_diff);
	zbx_vector_uint64_pair_destroy(&proxy_subscribtions);

	zbx_vector_ptr_destroy(&trigger_timers);
	zbx_vector_uint64_destroy(&triggerids);
}

/******************************************************************************
 *                                                                            *
 * Purpose: writes updates and new data from history cache to database        *
 *                                                                            *
 * Parameters: values_num - [OUT] the number of synced values                  *
 *             more      - [OUT] a flag indicating the cache emptiness:       *
 *                                ZBX_SYNC_DONE - nothing to sync, go idle    *
 *                                ZBX_SYNC_MORE - more data to sync           *
 *                                                                            *
 ******************************************************************************/
void zbx_sync_history_cache(int *values_num, int *triggers_num, int *more, int proc_num)
{
	zabbix_log(LOG_LEVEL_DEBUG, "In %s() history_num ", __func__);

	*values_num = 0;
	*triggers_num = 0;

	if (0 != (program_type & ZBX_PROGRAM_TYPE_SERVER))
		sync_server_history(values_num, triggers_num, proc_num);
	else
		sync_proxy_history(values_num, proc_num);
}

// int history_update_log_enty_severity(ZBX_DC_HISTORY *h, int severity, u_int64_t eventid, u_int64_t triggerid, int value) {
	
// 	if  (ITEM_VALUE_TYPE_LOG != h->value_type)
// 		return FAIL;
	
// 	if ( 1 == value) { //remembering max severity
// 	   if ( TRIGGER_SEVERITY_UNDEFINED != h->value.log->severity &&
// 		    h->value.log->severity >= severity)
// 			return FAIL;
// 		h->value.log->severity = severity;
// 	} else { //recovery data, for it severity is 0 which is OK
// 		h->value.log->severity = 0; // indication of recovery or OK value
// 	}	

// 	h->value.log->logeventid = eventid;
// 	char *buff = zbx_malloc(NULL, MAX_ID_LEN);
// 	zbx_snprintf(buff, MAX_ID_LEN, "%ld", triggerid);
	
// 	h->value.log->source = buff;
	
// 	return SUCCEED;
// }

ZBX_SHMEM_FUNC_IMPL(__hc_index, hc_index_mem)
/******************************************************************************
 *                                                                            *
 * Purpose: Allocate shared memory for database cache                         *
 *                                                                            *
 ******************************************************************************/
int init_database_cache(char **error)
{
	int ret;

	zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

	if (SUCCEED != (ret = zbx_mutex_create(&cache_ids_lock, ZBX_MUTEX_CACHE_IDS, error)))
		goto out;

	if (SUCCEED != (ret = zbx_shmem_create(&hc_index_mem, 16 * ZBX_MEBIBYTE, "history index cache",
	 									   "HistoryIndexCacheSize", 0, error)))
	 {
	 	goto out;
	 }

	ids = (ZBX_DC_IDS *)__hc_index_shmem_malloc_func(NULL, sizeof(ZBX_DC_IDS));
		memset(ids, 0, sizeof(ZBX_DC_IDS));

	if (NULL == sql)
		sql = (char *)zbx_malloc(sql, sql_alloc);
out:
	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);

	return ret;
}

/******************************************************************************
 *                                                                            *
 * Purpose: Free memory allocated for database cache                          *
 *                                                                            *
//  ******************************************************************************/
 void free_database_cache(int sync)
 {
 	zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

 	zbx_shmem_destroy(hc_index_mem);
 	hc_index_mem = NULL;

 	zbx_mutex_destroy(&cache_ids_lock);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
 }

/******************************************************************************
 *                                                                            *
 * Purpose: Return next id for requested table                                *
 *                                                                            *
 ******************************************************************************/
zbx_uint64_t DCget_nextid(const char *table_name, int num)
{
	int i;
	DB_RESULT result;
	DB_ROW row;
	const ZBX_TABLE *table;
	ZBX_DC_ID *id;
	zbx_uint64_t min = 0, max = ZBX_DB_MAX_ID, nextid, lastid;

	LOG_DBG("In %s() table:'%s' num:%d", __func__, table_name, num);

	LOCK_CACHE_IDS;

	for (i = 0; i < ZBX_IDS_SIZE; i++)
	{
		id = &ids->id[i];
		if ('\0' == *id->table_name)
			break;

		if (0 == strcmp(id->table_name, table_name))
		{
			nextid = id->lastid + 1;
			id->lastid += num;
			lastid = id->lastid;

			UNLOCK_CACHE_IDS;

			zabbix_log(LOG_LEVEL_DEBUG, "End of %s() table:'%s' [" ZBX_FS_UI64 ":" ZBX_FS_UI64 "]",
					   __func__, table_name, nextid, lastid);

			return nextid;
		}
	}

	if (i == ZBX_IDS_SIZE)
	{
		zabbix_log(LOG_LEVEL_ERR, "insufficient shared memory for ids");
		exit(EXIT_FAILURE);
	}

	table = zbx_db_get_table(table_name);

	result = zbx_db_select("select max(%s) from %s where %s between " ZBX_FS_UI64 " and " ZBX_FS_UI64,
					  table->recid, table_name, table->recid, min, max);

	if (NULL != result)
	{
		zbx_strlcpy(id->table_name, table_name, sizeof(id->table_name));

		if (NULL == (row = zbx_db_fetch(result)) || SUCCEED == zbx_db_is_null(row[0]))
			id->lastid = min;
		else
			ZBX_STR2UINT64(id->lastid, row[0]);

		nextid = id->lastid + 1;
		id->lastid += num;
		lastid = id->lastid;
	}
	else
		nextid = lastid = 0;

	UNLOCK_CACHE_IDS;

	zbx_db_free_result(result);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s() table:'%s' [" ZBX_FS_UI64 ":" ZBX_FS_UI64 "]",
			   __func__, table_name, nextid, lastid);

	return nextid;
}

