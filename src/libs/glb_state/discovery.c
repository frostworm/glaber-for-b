/*
** Copyright Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

#include "zbxcommon.h"

#include "zbxalgo.h"
#include "glb_state.h"
#include "glb_lock.h"
#include "load_dump.h"

typedef struct {
    const char *row;
    int lifetime;
    int delete_time;
} discovery_row_t;

typedef struct {
    u_int64_t itemid;
    zbx_hashset_t discoveries;
} discovery_item_t;

typedef struct {
    elems_hash_t *items;
    mem_funcs_t memf;
    strpool_t strpool;
} conf_t;

static conf_t *conf = NULL;

ELEMS_CREATE(discovery_create_cb){
    LOG_INF("In the callback");
    discovery_row_t *new_row, *row = (discovery_row_t*)data;

    }

ELEMS_FREE(discovery_free_cb) {
    discovery_row_t *row = (discovery_row_t *)elem->data;
    
    strpool_free(&conf->strpool, row->row);
    conf->memf.free_func(row);
}


int discovery_init(mem_funcs_t *memf)
{
    if (NULL == (conf = (conf_t*)memf->malloc_func(NULL, sizeof(conf_t)))) {
        LOG_WRN("Cannot allocate memory for cache struct");
        exit(-1);
    };
    
    strpool_init(&conf->strpool, memf);
    
  return SUCCEED;
}


int glb_state_discovery_if_row_needs_processing(u_int64_t itemid, struct zbx_json_parse *jp_row, int lifetime) {
 
    return SUCCEED;
}
