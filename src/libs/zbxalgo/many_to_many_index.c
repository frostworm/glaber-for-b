//glb_index is set of methods to manupulate 
// id1->id2 indexes whith back-linking to allow correctly
// id1 ( keys) and id2 ( values )
// it's based on glb_elems implementation to work in shm with as less locking as possible
#include "zbxcommon.h"
#include "zbxalgo.h"
#include "log.h"

struct many_to_many_index_t{
    elems_hash_t *idx_1_to_2;
    elems_hash_t *idx_2_to_1;
    mem_funcs_t memf;
} ;

typedef struct {
    zbx_vector_uint64_t refs;
 } ref_id_t;


ELEMS_FREE(ref_free_callback){
    ref_id_t *ref = (ref_id_t *)elem->data;

    zbx_vector_uint64_destroy(&ref->refs);
    (*memf->free_func)(ref);
}


ELEMS_CREATE(ref_create_callback) {
   
    ref_id_t *ref = (ref_id_t *)(*memf->malloc_func)(NULL,sizeof(ref_id_t));
    elem->data = ref;
    zbx_vector_uint64_create_ext(&ref->refs, memf->malloc_func, memf->realloc_func, memf->free_func);

}

many_to_many_index_t *many_to_many_index_init(mem_funcs_t *memf) {
    mem_funcs_t local_memf = { .malloc_func = ZBX_DEFAULT_MEM_MALLOC_FUNC, .realloc_func = ZBX_DEFAULT_MEM_REALLOC_FUNC, .free_func = ZBX_DEFAULT_MEM_FREE_FUNC };

    if (NULL == memf) 
        memf=&local_memf;
        
    many_to_many_index_t *idx =(many_to_many_index_t *)memf->malloc_func(NULL, sizeof(many_to_many_index_t));
       
    idx->idx_1_to_2 = elems_hash_init(memf, ref_create_callback, ref_free_callback);
    idx->idx_2_to_1 = elems_hash_init(memf, ref_create_callback, ref_free_callback);

    idx->memf = *memf;
    return idx;
}

void many_to_many_index_destroy(many_to_many_index_t *idx) {
    elems_hash_destroy(idx->idx_1_to_2);
    elems_hash_destroy(idx->idx_2_to_1);    
        
    (*idx->memf.free_func)(idx);
}

 ELEMS_CALLBACK(add_ref_nosort_callback) {
    
     ref_id_t *ref = (ref_id_t *)elem->data;
     u_int64_t *id_to = (u_int64_t *)data;
  
     zbx_vector_uint64_append(&ref->refs, *id_to);
     return SUCCEED;
 }


ELEMS_CALLBACK(add_ref_callback) {
    
    ref_id_t *ref = (ref_id_t *)elem->data;
    u_int64_t *id_to = (u_int64_t *)data;
  
    zbx_vector_uint64_append(&ref->refs, *id_to);
    zbx_vector_uint64_sort(&ref->refs, ZBX_DEFAULT_UINT64_COMPARE_FUNC);    
    return SUCCEED;
}

ELEMS_CALLBACK(del_ref_callback){
    int i;
    u_int64_t id_to = *(u_int64_t *)data;
    ref_id_t *ref = (ref_id_t *)elem->data;

    if (FAIL == (i = zbx_vector_uint64_bsearch(&ref->refs, id_to, ZBX_DEFAULT_UINT64_COMPARE_FUNC)))
        return SUCCEED;
    
    zbx_vector_uint64_remove(&ref->refs, i);
    
    if (0 == ref->refs.values_num)
        elem->flags |= ELEM_FLAG_DELETE;
    return SUCCEED;
}

static int get_refs_callback(elems_hash_elem_t *elem, mem_funcs_t *memf, void *params) {
    zbx_vector_uint64_t *out_refs = (zbx_vector_uint64_t*) params;
    ref_id_t *ref = (ref_id_t *) elem->data;
    
    zbx_vector_uint64_reserve(out_refs, ref->refs.values_num);
    zbx_vector_uint64_append_array(out_refs, ref->refs.values, ref->refs.values_num);
    
    return SUCCEED;
}

int many_to_many_index_del_id_1(many_to_many_index_t* idx, u_int64_t id1) {
    int i;
    zbx_vector_uint64_t ids2;
  
    zbx_vector_uint64_create(&ids2);
    
    //finding 'to' ids which will have references to the 'from' in idx_2_to_1 hash
    elems_hash_process(idx->idx_1_to_2, id1, get_refs_callback, &ids2, ELEM_FLAG_DO_NOT_CREATE);
     
    //deleting back reference from all the ids collected
    for (i = 0; i< ids2.values_num; i++) {
        elems_hash_process(idx->idx_2_to_1, ids2.values[i], del_ref_callback, &id1, ELEM_FLAG_DO_NOT_CREATE);
    }
 
    zbx_vector_uint64_destroy(&ids2);
    //removing the id1
    elems_hash_delete(idx->idx_1_to_2, id1);
   
    return SUCCEED;
}

int many_to_many_index_del_id_2(many_to_many_index_t* idx, u_int64_t id2) {
    int i;
    zbx_vector_uint64_t ids1;
  
    zbx_vector_uint64_create(&ids1);
    
    //finding 'from' ids which will have references to the 'to' in from_to hash
    elems_hash_process(idx->idx_2_to_1, id2, get_refs_callback, &ids1, ELEM_FLAG_DO_NOT_CREATE);
     
    //deleting back reference from all the ids collected
    for (i = 0; i< ids1.values_num; i++) {
        elems_hash_process(idx->idx_1_to_2, ids1.values[i], del_ref_callback, &id2, ELEM_FLAG_DO_NOT_CREATE);
    }
 
    zbx_vector_uint64_destroy(&ids1);
    //removing the id
    elems_hash_delete(idx->idx_2_to_1, id2);
   
    return SUCCEED;

}

int many_to_many_index_add_1_to_2(many_to_many_index_t* idx, u_int64_t id_1, u_int64_t id_2) {
    
    elems_hash_process(idx->idx_1_to_2, id_1, add_ref_callback, &id_2, 0);
    elems_hash_process(idx->idx_2_to_1, id_2, add_ref_callback, &id_1, 0);
    
    return SUCCEED;    
}

int obj_index_add_ref_nosort(many_to_many_index_t* idx, u_int64_t id_1, u_int64_t id_2) {
    
    elems_hash_process(idx->idx_1_to_2, id_1, add_ref_nosort_callback, &id_2, 0);
    elems_hash_process(idx->idx_2_to_1, id_2, add_ref_nosort_callback, &id_1, 0);
    
    return SUCCEED;    
}


int many_to_many_index_del_1_to_2(many_to_many_index_t* idx, u_int64_t id_1, u_int64_t id_2) {
    
    elems_hash_process(idx->idx_1_to_2, id_1, del_ref_callback, &id_2, ELEM_FLAG_DO_NOT_CREATE );
    elems_hash_process(idx->idx_2_to_1, id_2, del_ref_callback, &id_1, ELEM_FLAG_DO_NOT_CREATE );
    
    return SUCCEED;
}

int many_to_many_index_get_refs_id_1(many_to_many_index_t *idx, u_int64_t id_1, zbx_vector_uint64_t *ids_2) {
    return  elems_hash_process(idx->idx_1_to_2, id_1, get_refs_callback, ids_2, ELEM_FLAG_DO_NOT_CREATE);
}

int many_to_many_index_get_refs_id_2(many_to_many_index_t *idx, u_int64_t id_2, zbx_vector_uint64_t *ids_1) {
    return  elems_hash_process(idx->idx_2_to_1, id_2, get_refs_callback, ids_1, ELEM_FLAG_DO_NOT_CREATE);
}

void many_to_many_index_replace(many_to_many_index_t *old_idx, many_to_many_index_t *new_idx) {
    elems_hash_replace(old_idx->idx_1_to_2, new_idx->idx_1_to_2);
    elems_hash_replace(old_idx->idx_2_to_1, new_idx->idx_2_to_1);
    (*new_idx->memf.free_func)(new_idx);
}


static void update_vector(zbx_vector_uint64_t * vector, zbx_vector_uint64_t * new_vector) {
    zbx_vector_uint64_clear(vector);
    zbx_vector_uint64_append_array(vector, new_vector->values, new_vector->values_num);
    zbx_vector_uint64_sort(vector, ZBX_DEFAULT_UINT64_COMPARE_FUNC);
}


ELEMS_UPDATE(update_func_cb) {
    ref_id_t *ref =(ref_id_t *) elem->data, *new_ref =(ref_id_t *) elem_new->data;
    
    update_vector(&ref->refs, &new_ref->refs);
    return SUCCEED;
}

int many_to_many_merge_index(many_to_many_index_t *idx, many_to_many_index_t *new_idx) {
    elems_hash_update(idx->idx_1_to_2, new_idx->idx_1_to_2, update_func_cb);
    elems_hash_update(idx->idx_2_to_1, new_idx->idx_2_to_1, update_func_cb);
    return SUCCEED;
}


ELEMS_CALLBACK(id_to_vector_dump_cb) {
    char *str = NULL;
    int i=0;
    
    size_t alloc = 0, offset= 0;
    zbx_vector_uint64_t *vals = (zbx_vector_uint64_t*)elem->data;
    
    zbx_snprintf_alloc(&str,&alloc, &offset,"Key: %ld -> [", elem->id);

    for (i=0; i<vals->values_num; i++) {
        zbx_snprintf_alloc(&str,&alloc, &offset,"%ld, ", vals->values[i]);
    }
    
    if (vals->values_num > 0)  //removing trailing comma and space
        offset = offset - 2;
    
    zbx_snprintf_alloc(&str,&alloc, &offset,"]");
 
    zbx_free(str);
    return SUCCEED;
}

void many_to_many_index_dump(many_to_many_index_t *idx) {
    LOG_INF("dump idx 1 -> 2 :");
    elems_hash_iterate(idx->idx_1_to_2, id_to_vector_dump_cb, NULL, ELEMS_HASH_READ_ONLY);
    LOG_INF("dump idx 2 -> 1 :");
    elems_hash_iterate(idx->idx_2_to_1, id_to_vector_dump_cb, NULL, ELEMS_HASH_READ_ONLY);
}

int many_to_many_index_get_numdata(many_to_many_index_t *idx) {
    return idx->idx_1_to_2->elems.num_data;
}