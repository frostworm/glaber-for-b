/*
** Glaber
** Copyright (C) 2018-2042 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

#include "log.h"
#include "zbxshmem.h"
#include "zbxalgo.h"

static  zbx_shmem_info_t	*shmtest_mem;
ZBX_SHMEM_FUNC_IMPL(__shmtest1, shmtest_mem);

#define TEST_MEM_SIZE 1 * ZBX_GIBIBYTE
#define TEST_OBJ_RECORDS 1000
#define TEST_ELEMS 50

static void test_shm_leak(void) {
    char *error = NULL;
    LOG_INF("Starting shared memory leak test");
    
    if (SUCCEED != zbx_shmem_create(&shmtest_mem, TEST_MEM_SIZE, "State cache size", "TestMemSize", 0, &error)) {
        zabbix_log(LOG_LEVEL_CRIT,"Shared memory create failed: %s", error);
    	exit(EXIT_FAILURE);
    }
    mem_funcs_t memf = {.malloc_func= __shmtest1_shmem_malloc_func,
        .realloc_func = __shmtest1_shmem_realloc_func,
        .free_func = __shmtest1_shmem_free_func};
    
    u_int64_t was_free = shmtest_mem->free_size;
    
    LOG_INF("Done init obj index, cache free space is %ld", was_free);
    
    many_to_many_index_t *idx = many_to_many_index_init(&memf);
   
    LOG_INF("Adding %d records", TEST_OBJ_RECORDS); 
    
    int i;
    
    for (i = 0; i< TEST_OBJ_RECORDS; i++) {
        many_to_many_index_add_1_to_2(idx, i, (rand()+1) % (TEST_OBJ_RECORDS+2) );
    }
   
   LOG_INF("After adding records free space is %ld", shmtest_mem->free_size);
 
   
    many_to_many_index_destroy(idx);
    LOG_INF("After removing records free space is %ld", shmtest_mem->free_size);

    if (was_free !=  shmtest_mem->free_size) {
        LOG_INF("Obj index add/remove leak is detected, %" PRIu64 "bytes is lost, TEST FAILED",
            was_free - shmtest_mem->free_size );
        
        exit(EXIT_FAILURE);
    } else 
        LOG_INF("Records create/delete TEST OK");

    idx = many_to_many_index_init(&memf);
   
    LOG_INF("Adding1 %d records", TEST_OBJ_RECORDS); 
    
    for (i = 0; i< TEST_OBJ_RECORDS; i++) {
        many_to_many_index_add_1_to_2(idx,i,i);
    }    

    many_to_many_index_t *idx2 = many_to_many_index_init(&memf);
   
    LOG_INF("Adding2 %d records", TEST_OBJ_RECORDS); 
    
    for (i = TEST_OBJ_RECORDS; i< TEST_OBJ_RECORDS*2; i++) {
        many_to_many_index_add_1_to_2(idx2,i,i);
    }    
    many_to_many_index_replace(idx,idx2);
    many_to_many_index_destroy(idx);

    if (was_free !=  shmtest_mem->free_size) {
        
        LOG_INF("Obj replace leak is detected, %" PRIu64 " bytes is lost, TEST FAILED",
            was_free - shmtest_mem->free_size );
        
        exit(EXIT_FAILURE);
    } else 
        LOG_INF("Records replace TEST OK");
    LOG_INF("Testing obj_index_update");
    
    many_to_many_index_t *idx_shmem = many_to_many_index_init(&memf);
    many_to_many_index_t *idx_local = many_to_many_index_init(NULL);
    
    for (i = 0; i< TEST_OBJ_RECORDS; i++) {
        many_to_many_index_add_1_to_2(idx_local,i,i*23);
    }    
    LOG_INF("Updating 1");
    //many_to_many_merge_index(idx_shmem, idx_local);
    LOG_INF("Updating 1 finished");
    LOG_INF("Updating 1 -same data");

    //many_to_many_merge_index(idx_shmem, idx_local);
    LOG_INF("Updating 1 -same data - finished");

    many_to_many_index_destroy(idx_local);
    idx_local = many_to_many_index_init(NULL);

    for (i = 0; i< TEST_OBJ_RECORDS; i++) {
        many_to_many_index_add_1_to_2(idx_local,i,i*2);
    }    
    LOG_INF("Updating 2");
    //many_to_many_merge_index(idx_shmem, idx_local);
    LOG_INF("Updating 2 - finished");
    
    many_to_many_index_destroy(idx_local);
    many_to_many_index_destroy(idx_shmem);
    
    if (was_free != shmtest_mem->free_size) {
        
        LOG_INF("Obj update leak is detected, %d bytes is lost, TEST FAILED",
            was_free - shmtest_mem->free_size );
        
        exit(EXIT_FAILURE);
    } else 
        LOG_INF("Records update TEST OK");
    
    LOG_INF("Finished testing obj_index_update");
    LOG_INF("Finished shared memory leak test");

}

void tests_obj_index_run() {
    LOG_INF("Running obj index tests");
    test_shm_leak();
    LOG_INF("Finished obj index tests");
}